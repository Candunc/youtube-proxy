<?php
function curl_get_file_size( $url ) { // "stolen" from http://stackoverflow.com/questions/2602612/php-remote-file-size-without-downloading-file
  // Assume failure.
  $result = -1;

  $curl = curl_init( $url );

  // Issue a HEAD request and follow any redirects.
  curl_setopt( $curl, CURLOPT_NOBODY, true );
  curl_setopt( $curl, CURLOPT_HEADER, true );
  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
  curl_setopt( $curl, CURLOPT_USERAGENT, get_user_agent_string() );

  $data = curl_exec( $curl );
  curl_close( $curl );

  if( $data ) {
    $content_length = "unknown";
    $status = "unknown";

    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
      $status = (int)$matches[1];
    }

    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
      $content_length = (int)$matches[1];
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    if( $status == 200 || ($status > 300 && $status <= 308) ) {
      $result = $content_length;
    }
  }

  return $result;
}

function err($var) {
	header('HTTP/1.1 303 See Other',true,303);
	header('Status: 303 See Other',true,303);
	if ($var == 1) {
		header('Location: index.php?err=Error: The following file type cannot be streamed. Download, or try a different quality.', true,303);
	} elseif ($var == 2) {
		header('Location: index.php?err=Error: Our backend (youtube-dl-api-server) is down. Please try again later.', true,303);
	} elseif ($var == 3) {
		header('Location: index.php?err=Error: Could not find your video, either the quality is too high, or the video doesn\'t exist.', true,303);
	} else {
		header('Location: index.php?err=Error: Something went wrong while processing your url. Please try again later.', true,303);
	}
	die();
}

if ( isset($_POST["url"]) && isset($_POST["quality"]) && isset($_POST["value"]) ) {
	$url = $_POST["url"]; $quality = $_POST["quality"]; $value = $_POST["value"];
} elseif ( isset($_GET["url"]) && isset($_GET["quality"]) && isset($_GET["value"]) ){
	$url = $_GET["url"]; $quality = $_GET["quality"]; $value = $_GET["value"];
} else {
	err(0);
	die();
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://youtube-dl.appspot.com/api/info?url=" . $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
if (hash('md5', $output) == "23e526ac19ee8babb1772cb227ff8e62") {
	err(2);
}
$array = json_decode($output,true);
curl_close($ch);
unset($ch);

if (isset($array['videos'][0]['formats']) != true) { err(0); }

$shit_went_wrong = 0;
$img = $array['videos'][0]['thumbnail'];
$vid = $array['videos'][0]['id'];
foreach ($array['videos'][0]['formats'] as $key) {
	if (substr($key['format'],(strlen($key['format_id'])+3)) == $quality) {
		if ($value == "Watch") {
			if ($key['ext'] == "3gp") {
				$shit_went_wrong = 1;
			} elseif ($key['ext'] == 'm4a') {
				$ext = 'audio/mp4';
				$video_url = $key['url'];
				break;
			} elseif ($quality == "audio only (DASH audio)" && $key['ext'] == 'webm') {
				$ext = 'audio/webm';
				$video_url = $key['url'];
				break;
			} else {
				$ext = ('video/' . $key['ext']);
				$video_url = $key['url'];
				break;
			}
		} elseif ($value == "Download") {
			$ext = $key['ext'];
			$video_url = $key['url'];
			break;
		}
	}
}
if (isset($video_url) == false) {
	if ($shit_went_wrong == 1) {
		err(1);
	} else {
		err(3); 
	}
}

if ($value == "Watch") {
	header('HTTP/1.1 303 See Other',true,303);
	header('Status: 303 See Other',true,303);
	header('Location: player.php?img=' . $img . '&ext=' . $ext . '&id=' . strtr($video_url, array('&' => ';')), true,303);
} elseif ($value == "Download") {
	header('Content-Type: application/octet-stream');
	header('Content-Transfer-Encoding: Binary');
	header('Content-Length: ' . curl_get_file_size($video_url));
	header('Content-disposition: attachment; filename="' . $vid . '.' . $ext . '"'); 
	readfile($video_url);
}
?>