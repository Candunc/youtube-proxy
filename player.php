<!doctype html><html lang="en-us">
<!--This website uses the following products:
youtube-dl				youtube-dl is released into the public domain by the copyright holders. (View Licence: https://github.com/rg3/youtube-dl/blob/master/LICENSE)
youtube-dl-api-server	youtube-dl-api-server is released into the public domain 				(View Licence: https://github.com/jaimeMF/youtube-dl-api-server/blob/master/LICENSE)
video.js				Copyright 2014 Brightcove, Inc. 										(View Licence: https://github.com/videojs/video.js/blob/master/LICENSE)
-->
<head>
	<title>View YouTube videos anywhere!</title>

	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="robots" content="index,nofollow" />
	<meta name="googlebot" content="index,nofollow" />
	<meta name="description" content="A way to get around certain region-locked videos, or videos that are banned by your Network Administrator"/>

	<link rel="stylesheet" type="text/css" href="main.css">
	<link href="//vjs.zencdn.net/4.5/video-js.css" rel="stylesheet">
	<script src="//vjs.zencdn.net/4.5/video.js"></script>
</head>
<body>
<div id="player">
<?php if (isset($_GET['id']) && ($_GET['id'] != '')) { echo('<video id="Default_Player" class="video-js vjs-default-skin" controls preload="none" width="960" height="540" data-setup="{}" poster="' . $_GET['img'] . '"> <source src="' . strtr($_GET['id'], array (';' => '&')) . '" type=\'' . $_GET['ext'] . '\'> <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>'); } else { echo('<p>An error has occured, your video could not be loaded</p>'); } ?>
</div>
</body>
</html>