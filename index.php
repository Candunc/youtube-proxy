<!doctype html><html lang="en-us">
<!--This website uses the following products:
youtube-dl				youtube-dl is released into the public domain by the copyright holders. (View Licence: https://github.com/rg3/youtube-dl/blob/master/LICENSE)
youtube-dl-api-server	youtube-dl-api-server is released into the public domain 				(View Licence: https://github.com/jaimeMF/youtube-dl-api-server/blob/master/LICENSE)
video.js				Copyright 2014 Brightcove, Inc. 										(View Licence: https://github.com/videojs/video.js/blob/master/LICENSE)
-->
<head>
	<title>View YouTube videos anywhere!</title>

	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="robots" content="index,nofollow" />
	<meta name="googlebot" content="index,nofollow" />
	<meta name="description" content="A way to get around certain region-locked videos, or videos that are banned by your Network Administrator"/>

	<link rel="stylesheet" type="text/css" href="main.css">
	<?php if (isset($_GET['err']) && $_GET['err'] != "") { echo('<script>alert("' . $_GET['err'] . '")</script>'); } ?>
</head>
<body>
<div id="main">
	<form method="POST" action="form.php">
	<input type="text" name="url" value="" size="50">
		<select name="quality">
			<option value="176x144">144p</option>
			<option value="320x240">240p</option>
			<option value="640x360">360p</option>
			<option value="1280x720">720p</option>
			<option value="audio only (DASH audio)">Audio</option>
		</select>
		<select name="value">
			<option value="Watch">Watch</option>
			<option value="Download">Download</option>
		</select>
		<input type="submit" value="Submit">
	</form>
</div>
</body>
</html>